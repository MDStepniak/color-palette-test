const imgArray = new Array();

imgArray[0] = new Image();
imgArray[0].src = './images/color_palette_1.png';

imgArray[1] = new Image();
imgArray[1].src = './images/color_palette_2.png';

imgArray[2] = new Image();
imgArray[2].src = './images/color_palette_3.png';

imgArray[3] = new Image();
imgArray[3].src = './images/color_palette_4.png';

imgArray[4] = new Image();
imgArray[4].src = './images/color_palette_5.png';

imgArray[5] = new Image();
imgArray[5].src = './images/color_palette_6.png';

imgArray[6] = new Image();
imgArray[6].src = './images/color_palette_7.png';

imgArray[7] = new Image();
imgArray[7].src = './images/color_palette_8.png';

imgArray[8] = new Image();
imgArray[8].src = './images/color_palette_9.png';

imgArray[9] = new Image();
imgArray[9].src = './images/color_palette_10.png';

imgArray[10] = new Image();
imgArray[10].src = './images/color_palette_11.png';



function nextImage(){
    let img = document.getElementById("mainImage");
    for(let i = 0; i < imgArray.length;i++){
        if(imgArray[i].src == img.src){
            if(i === imgArray.length){
                document.getElementById("mainImage").src = imgArray[0].src;
                break;
            }
            document.getElementById("mainImage").src = imgArray[i+1].src;
            break;
        }
    }
}
function previousImage(){
    let img = document.getElementById("mainImage");
    for(let i = imgArray.length-1; i >=0 ;i--){
        if(imgArray[i].src == img.src){
            if(i === imgArray.length){
                document.getElementById("mainImage").src = imgArray[4].src;
                break;
            }
            document.getElementById("mainImage").src = imgArray[i-1].src;
            break;
        }
    }
}

